# CKEditor 5 Tooltip for Drupal 10 (9.3.0-beta1 and up) with bootstrap based theme
Drupal 10 CKEditor 5 Plugin for adding tooltips direclty in the WYSIWYG editor.

###Install Process:
Clone into /drupalroot/modules/custom, then navigate to yoursite.com/admin/modules
and activate the module in the GUI.

###Pre reqs:
* A Bootstrap-based theme
* Libraries Module

###Restrictions:
If HTML restriction is on (i.e. if you try to add this button to a "Basic HTML"
text editor style) it will crash the editor window on load, so don't do that.
Works completely fine on Full HTML.
