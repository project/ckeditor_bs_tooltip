import BsTooltipEditing from './bstooltip_editing';
import BsTooltipUI from './bstooltip_ui';
import { Plugin } from 'ckeditor5/src/core';

export default class BsTooltip extends Plugin {
  static get requires() {
    return [BsTooltipEditing, BsTooltipUI];
  }
}
