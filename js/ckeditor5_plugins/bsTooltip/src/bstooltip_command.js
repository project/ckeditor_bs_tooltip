import { Command } from 'ckeditor5/src/core';
import { findAttributeRange } from 'ckeditor5/src/typing';
import { toMap } from 'ckeditor5/src/utils';
import getRangeText from './utils.js';

export default class BsTooltipCommand extends Command {
  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;
    const firstRange = selection.getFirstRange();

    if ( firstRange.isCollapsed ) {
      if ( selection.hasAttribute( 'bsTooltip' ) ) {
        const attributeValue = selection.getAttribute( 'bsTooltip' );

        const abbreviationRange = findAttributeRange(
          selection.getFirstPosition(), 'bsTooltip', attributeValue, model
        );

        this.value = {
          abbr: getRangeText( abbreviationRange ),
          title: attributeValue,
          range: abbreviationRange
        };
      } else {
        this.value = null;
      }
    }
    else {
      if ( selection.hasAttribute( 'bsTooltip' ) ) {
        const attributeValue = selection.getAttribute( 'bsTooltip' );

        // Find the entire range containing the abbreviation
        // under the caret position.
        const abbreviationRange = findAttributeRange(
          selection.getFirstPosition(), 'bsTooltip', attributeValue, model
        );

        if ( abbreviationRange.containsRange( firstRange, true ) ) {
          this.value = {
            abbr: getRangeText( firstRange ),
            title: attributeValue,
            range: firstRange
          };
        } else {
          this.value = null;
        }
      } else {
        this.value = null;
      }
    }

    this.isEnabled = model.schema.checkAttributeInSelection(
      selection, 'bsTooltip'
    );
  }

  execute( { title, abbr } ) {
    const model = this.editor.model;
    const selection = model.document.selection;

    model.change( writer => {
      if ( selection.isCollapsed ) {
        if ( this.value ) {
          const { end: positionAfter } = model.insertContent(
            writer.createText( abbr, { abbreviation: title } ),
            this.value.range
          );

          writer.setSelection( positionAfter );
        }
        else if ( abbr !== '' ) {
          const firstPosition = selection.getFirstPosition();

          const attributes = toMap( selection.getAttributes() );

          attributes.set( 'bsTooltip', title );

          const { end: positionAfter } = model.insertContent(
            writer.createText( abbr, attributes ), firstPosition
          );

          writer.setSelection( positionAfter );
        }

        writer.removeSelectionAttribute( 'bsTooltip' );
      } else {
        const ranges = model.schema.getValidRanges(
          selection.getRanges(), 'bsTooltip'
        );

        for ( const range of ranges ) {
          writer.setAttribute( 'bsTooltip', title, range );
        }
      }
    } );
  }
}
