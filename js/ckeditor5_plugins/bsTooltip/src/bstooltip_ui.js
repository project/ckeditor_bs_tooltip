import { Plugin } from 'ckeditor5/src/core';
import { ButtonView } from 'ckeditor5/src/ui';
import { ContextualBalloon, clickOutsideHandler } from 'ckeditor5/src/ui';
import FormView from './bstooltip_view';
import BsTooltipIcon from '../../../../icons/comment-alt-regular.svg';
import getRangeText from './utils.js';

export default class BsTooltipUI extends Plugin {
  static get requires() {
    return [ ContextualBalloon ];
  }
  init() {
    const editor = this.editor;

    this._balloon = this.editor.plugins.get( ContextualBalloon );
    this.formView = this._createFormView();

    editor.ui.componentFactory.add('bsTooltip', () => {
      const button = new ButtonView();

      button.label = 'Tooltip';
      button.icon = BsTooltipIcon;
      button.tooltip = true;
      button.withText = false;

      this.listenTo( button, 'execute', () => {
        this._showUI();
      } );

      return button;
    });
  }

  _createFormView() {
    const editor = this.editor;
    const formView = new FormView( editor.locale );

    this.listenTo( formView, 'submit', () => {
      const value = {
        abbr: formView.abbrInputView.fieldView.element.value,
        title: formView.titleInputView.fieldView.element.value
      };

      editor.execute( 'addBsTooltip', value );

      this._hideUI();
    } );

    this.listenTo( formView, 'cancel', () => {
      this._hideUI();
    } );

    clickOutsideHandler( {
      emitter: formView,
      activator: () => this._balloon.visibleView === formView,
      contextElements: [ this._balloon.view.element ],
      callback: () => this._hideUI()
    } );

    formView.keystrokes.set( 'Esc', ( data, cancel ) => {
      this._hideUI();
      cancel();
    } );

    return formView;
  }

  _showUI() {
    const selection = this.editor.model.document.selection;

    const commandValue = this.editor.commands.get( 'addBsTooltip' ).value;

    this._balloon.add( {
      view: this.formView,
      position: this._getBalloonPositionData()
    } );

    this.formView.abbrInputView.isEnabled = selection.getFirstRange().isCollapsed;

    if ( commandValue ) {
      this.formView.abbrInputView.fieldView.value = commandValue.abbr;
      this.formView.titleInputView.fieldView.value = commandValue.title;
    }
    else {
      const selectedText = getRangeText( selection.getFirstRange() );

      this.formView.abbrInputView.fieldView.value = selectedText;
      this.formView.titleInputView.fieldView.value = '';
    }

    this.formView.focus();
  }

  _hideUI() {
    this.formView.abbrInputView.fieldView.value = '';
    this.formView.titleInputView.fieldView.value = '';
    this.formView.element.reset();

    this._balloon.remove( this.formView );

    this.editor.editing.view.focus();
  }

  _getBalloonPositionData() {
    const view = this.editor.editing.view;
    const viewDocument = view.document;
    let target = null;

    target = () => view.domConverter.viewRangeToDom( viewDocument.selection.getFirstRange() );

    return {
      target
    };
  }
}
