import { Plugin } from 'ckeditor5/src/core';
import BsTooltipCommand from "./bstooltip_command";

export default class BsTooltipEditing extends Plugin {
  init() {
    this._defineSchema();
    this._defineConverters();

    this.editor.commands.add(
      'addBsTooltip', new BsTooltipCommand( this.editor )
    );
  }
  _defineSchema() {
    const schema = this.editor.model.schema;

    schema.extend( '$text', {
      allowAttributes: [ 'bsTooltip' ]
    } );
  }
  _defineConverters() {
    const conversion = this.editor.conversion;

    conversion.for( 'downcast' ).attributeToElement( {
      model: 'bsTooltip',

      view: ( modelAttributeValue, conversionApi ) => {
        const { writer } = conversionApi;
        return writer.createAttributeElement( 'span', { //was abbr
          title: modelAttributeValue,
          class: 'bs_tooltip',
          'data-toggle':"tooltip",
          'data-placement': "top"
        } );
      }
    } );

    conversion.for( 'downcast' ).attributeToElement( {
      model: 'bsTooltip',

      view: ( modelAttributeValue, conversionApi ) => {
        const { writer } = conversionApi;
        return writer.createAttributeElement( 'span', { //was abbr
          title: modelAttributeValue
        } );
      }
    } );

    conversion.for( 'upcast' ).elementToAttribute( {
      view: {
        name: 'span', //was abbr
        attributes: [ 'title' ]
      },
      model: {
        key: 'bsTooltip',

        value: viewElement => {
          const title = viewElement.getAttribute( 'title' );
          return title;
        }
      }
    } );
  }
}
